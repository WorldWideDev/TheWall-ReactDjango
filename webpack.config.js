const path = require('path');
const BundleTracker = require('webpack-bundle-tracker');
module.exports = {
    devtool: "source-map",
    entry: "./client/src/index.tsx",
    output: {
        filename: "app.bundle.js",
        path: path.resolve(__dirname, "./client/dist/"),
        publicPath: 'http://localhost:8080/client/dist/',
    },
    plugins: [
        new BundleTracker({filename: './webpack-stats.json'}),
    ],
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", "jsx", ".json", "css"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, use: "awesome-typescript-loader" },
            { test: /\.css$/, use: ["style-loader", "css-loader"]
            },
        ]
    },
    devServer: {
        historyApiFallback: true,
        contentBase: "./",
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    }
};
