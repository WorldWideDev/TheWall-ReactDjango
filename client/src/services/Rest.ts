import ROOT_API_URL from './Api';
import 'isomorphic-fetch';
import DjangoHeaders from '../services/DjangoUtils';

export interface IError{
    error: string;
    error_content: string;
    [key:string]:string;
}
export interface IResponse<T> {
    is_error: boolean;
    error_content?: IError;
    content?: T
}

export default class RestService {
    /** makes generic request to API, takes the following:
     * route:string, 
     * model:T = null, 
     * method:string = "GET",
     * authRequired:boolean = true)
     * 
     * => Promise<IResponse<T>>
    */
    request<T>(
        route:string, 
        model:T = null, 
        method:string = "GET",
        authRequired:boolean = true):Promise<IResponse<T>>{
        const body = (!!model) ? JSON.stringify(model) : null;
        let isBadRequest = false;        
        return fetch(`${ROOT_API_URL}/${route}/`, {
            method: method,
            headers: DjangoHeaders(authRequired),
            body: body,
            credentials: "same-origin"
        }).then(response => {
            isBadRequest = (response.status == 400);
            let responseContentType = response.headers.get("content-type");
            if(responseContentType && responseContentType.indexOf("application/json") !== -1) {
                return response.json();
            } else {
                return response.text();
            }
        }).then((responseContent: any) => {
            let response:IResponse<T> = {
                is_error: isBadRequest,
                error_content: isBadRequest ? responseContent : null,
                content: isBadRequest ? null : responseContent
            };
            return response;
        });
    }
}
