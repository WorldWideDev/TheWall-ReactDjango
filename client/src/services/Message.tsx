import {IUser} from './User';
import RestService from './Rest';

export interface IMessage {
    id?:number
    content?:string;
    creator_id?:number;
    created_at?:string;
    updated_at?:string;
}

export interface IComment extends IMessage {
    message_id?:number;
    message?:IMessage;
    creator?:IUser;
}

const restService = new RestService();
export default class MessageService {
    getCommentsAsync(){
        return restService.request("comment");
    }
    getMessagesAsync(){
        return restService.request("message");
    }
    createComment(comment:IComment){
        return restService.request("comment", comment, "POST")
    }
    create(message:IMessage){
        return restService.request("message", message, "POST")
    }
}

export const formatDateTime = (dateString:string):string => {
    const date = new Date(dateString);
    return `${date.toLocaleDateString()} at ${date.toLocaleTimeString()}`;
}
