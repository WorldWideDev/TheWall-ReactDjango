import AuthStore from '../stores/Auth';
import RestService, {IError,IResponse} from './Rest';
export interface IUser {
    id?: number;
    first_name?: string;
    last_name?: string;
    email?: string;
    password?:string;
    confirm?:string;
    token?:string;
}
export const UserFieldMap:any = {
   first_name: {type:"text"}, 
   last_name: {type:"text"}, 
   email: {type:"email"}, 
   password: {type:"password"}, 
   confirm: {
       type:"password",
       display: "Confirm Password"
   }, 
}

const restService = new RestService();

export default class UserService {
    static isSignedIn():boolean {
        return !!(AuthStore.getToken());
    }
    signOut():void {
        AuthStore.removeToken();
    }
    loginOrRegister(user:IUser):Promise<IResponse<IUser>> {
        return restService.request(("first_name" in user) ? "user" : "get_auth_token", user, 
            "POST",
            // isAuthRequired??
            false)
    }
    loggedInUserId():Promise<IResponse<IUser>> {
         return restService.request("get_logged_in")
     }
}
