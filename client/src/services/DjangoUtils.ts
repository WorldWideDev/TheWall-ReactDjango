import 'isomorphic-fetch';
import cookie from 'react-cookie';
import AuthStore from '../stores/Auth';
const prepareHeaders = (authRequired:boolean):Headers =>
{
    const myHeaders = new Headers();
    myHeaders.append("X-CSRFToken", cookie.load("csrftoken"));
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Content-Type", "application/json");
    if(authRequired)
        myHeaders.append("Authorization", `Token ${AuthStore.getToken()}`);
    return myHeaders;
}
export default prepareHeaders;
