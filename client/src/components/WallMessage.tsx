import * as React from 'react';
import {CommentForm} from './CommentForm';
import WallComment from './WallComment';
import MessageService, {IMessage, IComment, formatDateTime} from '../services/Message';

const messageService = new MessageService();

interface WallMessageState {
    comments: IComment[]
}
export default class WallMessage extends React.Component<any, WallMessageState>{
    constructor(props:any){
        super(props);
        this.state = {
            comments:[]
        }
        this.fetchComments();
    }
    fetchComments = ()=> {
        messageService.getCommentsAsync().then(response => {

            this.setState({ 
                comments: response.content as IComment[]
            })
        });
    }
    render(){
        return (
            <div className="wall-post">
                <h4>{this.props.message.creator.first_name} wrote:</h4>
                <h3>{this.props.message.content}</h3>
                <h4>{formatDateTime(this.props.message.created_at)}</h4>
                <CommentForm onSuccess={this.fetchComments}
                             messageId={this.props.message.id}/>
                <div className="comments">
                    {this.state.comments
                        .filter(comment => comment.message.id == this.props.message.id)
                        .map((comment:IComment) => 
                            (<WallComment {...comment} />))}
                </div>
            </div>
        )
    }
}

