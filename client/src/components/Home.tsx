import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import Nav from './Nav';
import UserForm from './UserForm';

import './styles.css';
export interface HelloProps {
    compiler:string; 
    framework:string;
}

export default class Home extends React.Component<RouteComponentProps<{}>, {}> {
    render() {
        return  <div>
                    <h1>Hello and WELCOME</h1>
                    <div className="register-form">
                        <UserForm isLogin={false} onSuccess={this.handleSuccess} />
                        <UserForm isLogin={true} onSuccess={this.handleSuccess} />
                    </div>
                </div>
    }
    handleSuccess = (route:string) => {
        this.props.history.push(route);
    }
}
