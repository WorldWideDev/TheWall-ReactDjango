import * as React from 'react';
import MessageService, {IMessage, IComment} from '../services/Message';
import FormGroup from './FormGroup';
import {BaseModel, FormState, FormProps} from './BaseForm';

const messageService = new MessageService();


export default class MessageForm extends React.Component<FormProps<IMessage>, FormState<IMessage>> {
    constructor(props:FormProps<IMessage>){
        super(props);
        this.state = {
            model: this.initModel(),
            errors: {} as {[key:string]:string}
        };
    }
    initModel():any{
        return {
            content:""
        }
    }
    onUpdateModel = (value:string, fieldName:string) => {
        const updatedField = {
            [fieldName]: value
        };
        this.setState({
            model: Object.assign(this.state.model, updatedField)
        })
    }
    onHandleSubmit = (event:any) => {
        event.preventDefault();
        messageService.create(this.state.model)
            .then(response => {
                if(response.is_error){
                   this.setState({errors:response.error_content}); 
                } else {
                    console.log("all good bout to handle this submit rhy here");
                    this.setState({model:this.initModel()});
                    this.props.onSuccess();
                }
            })
    }
    render(){
        return (
            <form className="user-form" onSubmit={this.onHandleSubmit}>
                {Object.keys(this.state.model)
                       .map((field:string, idx:number) =>
                       (<FormGroup 
                            key={idx}
                            field={field} 
                            errors={this.state.errors[field]}
                            type="text"
                            onUpdateInput={this.onUpdateModel}/>))}
                
                {!this.state.errors.non_field_errors ? null : 
                    Object.keys(this.state.errors.non_field_errors).map((error:string,idx:number) =>
                    <div key={idx} className="form-group-error">{this.state.errors.non_field_errors[error]}</div>)}
                <button className="btn btn-primary">Submit</button>
            </form>
        );
    }
}

