import * as React from 'react';

interface FormGroupProps {
    type:string;
    field:string;
    display?:string;
    errors:string[];
    onUpdateInput:(value:string,fieldName:string)=>void;
}
interface FormGroupState {
    value:string;
}

export default class FormGroup extends React.Component<FormGroupProps,FormGroupState>{
    constructor(props:FormGroupProps){
        super(props);
        this.state = {value:""};
    }
    onHandleInputChange(value:string){
        this.setState({value});
        this.props.onUpdateInput(value,this.props.field);
    }
    formatFieldName(fieldName:string){
        return fieldName.split("_").map((str) => str[0].toUpperCase() + str.slice(1)).join(" ")
    }
    render(){
        return (
             <div className="form-group">
                 <label className="col-2 col-form-label">
                    {(!!this.props.display) ? 
                        this.props.display : 
                        this.formatFieldName(this.props.field)}
                 </label>
                 <input 
                    className="form-control" 
                    onChange={event => this.onHandleInputChange(event.target.value)}
                    type={this.props.type} />
                 {!this.props.errors ? null : (
                     this.props.errors.map((error:string, idx:number) =>
                         (<div className="form-group-error" key={idx}>
                             {error}
                          </div>)
                     )
                 )}
             </div>
        );
    }
}


