export interface BaseModel<T>{
    content:string;
}

export interface FormState<T> {
    model: T;
    errors: any;
}

export interface FormProps<T> {
    onSuccess:()=>void;
}
