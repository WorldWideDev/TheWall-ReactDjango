import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Route, Redirect, Switch } from 'react-router-dom';
import Home from './Home';
import Nav from './Nav';
import Dashboard from './Dashboard';
import UserForm from './UserForm';
import RestService from '../services/User';
export class RoutePaths {
    public static Home:string = '/';
    public static Success:string = '/dashboard'
}

export default class Routes extends React.Component<any, any> {
    render() {
        return <Switch>
                   <Route exact path={RoutePaths.Home} component={Home} />
                   <DefaultLayout path={RoutePaths.Success} component={Dashboard} />
               </Switch>
    }
}

const DefaultLayout = ({ component: Component, ...rest }: { component: any, path: string, exact?: boolean }) => (
    <Route {...rest} render={(props:any) => (
        RestService.isSignedIn() ? (
            <div>
                <div className="container">
                    <Nav {...props} />
                    <Component {...props} />
                </div>
            </div>
        ) : (
                <Redirect to={{
                    pathname: RoutePaths.Home,
                    state: { from: props.location }
                }} />
            )
    )} />
);
