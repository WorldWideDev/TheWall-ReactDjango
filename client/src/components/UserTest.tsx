import * as React from 'react';
import 'isomorphic-fetch';
import ROOT_API_URL from '../services/Api';
import {IUser} from '../services/User';
interface UserState {
    user: IUser;
    userInput: string
}

export default class UserTest extends React.Component<any, UserState> {
    constructor(props:any) {
        console.log("HELLO")
        super(props);
        this.state = {
            user:this.initUser(),
            userInput: ""
        }
    }
    initUser():IUser{
        return {
            first_name: '',
            last_name: '',
            email: ''
        }
    }
   onFetchUser(){
       const url = `${ROOT_API_URL}/user/${this.state.userInput}`;
       fetch(url)
           .then(response => {
               if(response.status >= 400){
                   throw new Error("Bad response from server")
               }
               return response.json()
           })
       
           .then(data => {
               this.setState({
                   user: { first_name: data.first_name,
                           last_name: data.last_name,
                           email: data.email }
               })
           })
   }
    render(){
        return (
            <div>
            <h4>Fetch User by ID</h4>
            <input 
                type="number"
                onChange={e => this.setState({userInput:e.target.value})} />
            <button onClick={ () => { this.onFetchUser() }}>Fetch</button>
            <hr />
            <p><strong>First Name:</strong> {this.state.user.first_name}</p>
            <p><strong>Last Name:</strong> {this.state.user.last_name}</p>
            <p><strong>Email:</strong> {this.state.user.email}</p>
            </div>
        );
    }
}


