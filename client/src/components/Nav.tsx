import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { NavLink } from 'react-router-dom';
import UserService from '../services/User';

const userService = new UserService();
export default class NavBar extends React.Component<RouteComponentProps<{}>, {}> {
    constructor(props:RouteComponentProps<{}>){
        super(props)
    }
    handleSignout =()=> {
        this.props.history.push("/");  
        userService.signOut();
    }
    render(){
        return (
            <nav className="main-nav">
                <a href="#" onClick={this.handleSignout}>Sign Out</a>
            </nav>
        )
    }
}
