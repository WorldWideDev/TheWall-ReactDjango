import * as React from 'react';
import MessageService, {IComment} from '../services/Message';
import FormGroup from './FormGroup';
import {BaseModel, FormState, FormProps} from './BaseForm';

const messageService = new MessageService();

interface CommentFormProps {
    onSuccess:()=>void;
    messageId:number;
}

export class CommentForm extends React.Component<CommentFormProps, FormState<IComment>>{
    constructor(props:any){
        super(props);
        this.state = {
            model: this.initModel(),
            errors: {} as {[key:string]:string}
        };
    }
    initModel():any{
        return {
            content:"",
            message_id:this.props.messageId
        }
    }
    onUpdateModel = (value:string, fieldName:string) => {
        const updatedField = {
            [fieldName]: value
        };
        this.setState({
            model: Object.assign(this.state.model, updatedField)
        })
    }
    onHandleSubmit = (event:any) => {
        event.preventDefault();
        messageService.createComment(this.state.model)
            .then(response => {
                if(response.is_error){
                   this.setState({errors:response.error_content}); 
                } else {
                    console.log("all good bout to handle this submit rhy here");
                    this.setState({model:this.initModel()});
                    this.props.onSuccess();
                }
            })
    }
    render(){
        return (
            <form className="user-form" onSubmit={this.onHandleSubmit}>
                {Object.keys(this.state.model)
                       .filter((field:string, idx:number) => field != "message_id")
                       .map((field:string, idx:number) =>
                       (<FormGroup 
                            key={idx}
                            display="Leave a Comment"
                            field={field} 
                            errors={this.state.errors[field]}
                            type="text"
                            onUpdateInput={this.onUpdateModel}/>))}
                
                {!this.state.errors.non_field_errors ? null : 
                    Object.keys(this.state.errors.non_field_errors).map((error:string,idx:number) =>
                    <div key={idx} className="form-group-error">{this.state.errors.non_field_errors[error]}</div>)}
                <button className="btn btn-primary">Submit</button>
            </form>
        );
    }
}
