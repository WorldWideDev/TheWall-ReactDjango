import * as React from 'react';

interface TimerState {
    time:string;
}

export default class Timer extends React.Component<{}, TimerState> {
    constructor(props:any){
        super(props);
        this.state = {
            time: new Date().toLocaleTimeString()
        }
    }
    componentDidMount(){
        setInterval(()=> {
            this.setState({time: new Date().toLocaleTimeString()});
        },1000);
    }
    render() {
        return <h3>The time is: <strong>{this.state.time}</strong></h3>;
    }
}
