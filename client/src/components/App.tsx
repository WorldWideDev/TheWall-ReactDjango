import * as React from 'react';
import Timer from "./Timer";
import Home from "./Home";
import UserForm from "./UserForm";
import Routes from './Routes';
import {BrowserRouter as Router} from 'react-router-dom';

export default class App extends React.Component<{}, {}> {
    render(){
        return (
            <div>
            <Router>
                <Routes /> 
            </Router>
            </div>
        );
    }
}

