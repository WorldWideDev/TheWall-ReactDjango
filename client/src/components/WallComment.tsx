import * as React from 'react';
import {IComment, formatDateTime} from '../services/Message';

const WallComment = (comment:IComment) => {
    return (
        <div className="wall-comment">
            <p>{comment.content}</p>
            <p>Posted by {comment.creator.first_name} {comment.creator.last_name} on {formatDateTime(comment.created_at)}</p>
        </div>
    );
}
export default WallComment;
