import * as React from 'react';
import UserService, {IUser, UserFieldMap} from '../services/User';
import {IError} from '../services/Rest';
import FormGroup from './FormGroup';
import { RoutePaths } from './Routes';
import cookie from 'react-cookie';
import AuthStore from '../stores/Auth';

const userService = new UserService();

interface UserFormState {
    user: IUser;
    errors: any;
}

interface UserFormProps {
    isLogin:boolean;
    onSuccess:(route:string)=>any;
}

export default class UserForm extends React.Component<UserFormProps, UserFormState> {
    constructor(props:UserFormProps){
        super(props);
        this.state = {
            user: this.initUser(),
            errors: {} as {[key:string]:string}
        };
    };
    initUser():IUser{
        return (!this.props.isLogin) ? {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            confirm: '',
        } : {
            email: '',
            password: '',
        }
    }
    onUpdateUser = (value:string, fieldName:string) => {
        const updatedUserField = {
            [fieldName]: value
        };
        this.setState({
            user: Object.assign(this.state.user, updatedUserField)
        })
    }
    onHandleSubmit = (event:any) => {
        event.preventDefault();
        userService.loginOrRegister(this.state.user)
            .then(response => {
                if(response.is_error){
                   this.setState({errors:response.error_content}); 
                }
                else { 
                    console.log(`setting token: ${response.content.token}`)
                    AuthStore.setToken(response.content.token);
                    this.props.onSuccess(RoutePaths.Success) }
            })
    }
    render(){
        return (
            <form className="user-form" onSubmit={this.onHandleSubmit}>
                {Object.keys(this.state.user)
                       //.filter(field => field!="csrfmiddlewaretoken")
                       .map((field:string, idx:number) =>
                       (<FormGroup 
                            key={idx}
                            field={field} 
                            errors={this.state.errors[field]}
                            type={UserFieldMap[field]["type"]}
                            display={("display" in UserFieldMap[field]) ? 
                                    UserFieldMap[field]["display"] : null }
                            onUpdateInput={this.onUpdateUser}/>))}
                
                {!this.state.errors.non_field_errors ? null : 
                    Object.keys(this.state.errors.non_field_errors).map((error:string,idx:number) =>
                    <div key={idx} className="form-group-error">{this.state.errors.non_field_errors[error]}</div>)}
                <button className="btn btn-primary">Submit</button>
            </form>
        );
    }
}
