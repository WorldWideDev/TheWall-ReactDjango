import * as React from 'react';
import MessageService, { IMessage } from '../services/Message';
import WallMessage from './WallMessage';
import MessageForm from './MessageForm';
interface DashboardState {
    messages: IMessage[]
}
const messageService = new MessageService();
export default class Dashboard extends React.Component<{},DashboardState> {
    constructor(props:any){
        super(props);
        this.state = {
            messages: []
        } 
        this.fetchMessages();
    }
    //(messages:IMessage[])
    fetchMessages = ()=> {
        messageService.getMessagesAsync().then(response => {
            console.log(response.content, "in fetchMessages");
            this.setState({
                messages: response.content as IMessage[]
            });
        })
    }
    render() {
        return (
            <div>
                <h1>The Wall</h1>
                <MessageForm 
                    onSuccess={this.fetchMessages} />
                {this.state.messages.map((message:IMessage) => 
                    (<WallMessage key={message.id} message={message} />)
                )}
            </div>
        )
    }
}
