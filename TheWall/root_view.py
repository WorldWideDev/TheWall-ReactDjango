from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie

@ensure_csrf_cookie
def api_root(request):
    """
    Root api view, serves static index entry for react app
    """
    print(request.COOKIES)
    return render(request, "index.html")
