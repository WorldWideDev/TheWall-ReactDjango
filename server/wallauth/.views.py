# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .forms import RegistrationForm, UpdateUser
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm
from django.contrib.auth import forms
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.shortcuts import render, redirect, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, UpdateView
from django.views.generic.detail import DetailView
from .models import WallUser
import os

# Create your views here.
def index(req):

    context = {
        "reg_form": RegistrationForm(),
        "log_form": AuthenticationForm()
    }
    return render(req, "loginreg/index.html", context)

def logreg(req, rType):
    regform = RegistrationForm()
    logform = AuthenticationForm()
    valid = False
    if int(rType) is 1:
        regform = RegistrationForm(req.POST)
        if regform.is_valid():
            user = regform.save()
            valid = True

    else:
        logform = AuthenticationForm(data=req.POST)
        if logform.is_valid():
            valid = True
            user = logform.get_user()
    context = {
        "reg_form": regform,
        "log_form": logform
    }
    if not valid:
        return render(req, "loginreg/index.html", context)

    #NOTE(dev): we can log in either user from register or login
    login(req, user)
    
    return redirect('/dashboard')

def change_pw(req):
    context = {
        'form': PasswordResetForm()
    }
    if req.POST:
        try:
            print(email)
        except:
            print("i guess theres not email context??")
        users = PasswordResetForm(req.POST).get_users(req.POST['email'])
        print(users)
    return render(req, "loginreg/changepw.html", context)

def log_out(req):
    logout(req)
    return redirect('/')

class ListUsers(ListView):
    pass

class UpdateUser(UpdateView):
    model = WallUser
    template_name_suffix = '_update'
    form_class = UpdateUser

    def form_valid(self, form):
        instance = form.save(commit=False)
        return super(UpdateUser, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(UpdateUser, self).get_context_data(**kwargs)        
        context['id'] = self.get_object().id
        return context

    def get_success_url(self):

        return reverse_lazy("user:show", kwargs={'pk': self.get_object().id})

class DetailUser(DetailView):
    model = WallUser
    def get_context_data(self, **kwargs):
        context = super(DetailUser, self).get_context_data(**kwargs)
        context['first_post'] = self.get_object().post_set.all().order_by('-created_at').first().content
        return context
