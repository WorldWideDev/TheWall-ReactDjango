from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from rest_framework.compat import authenticate
from .models import WallUser
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from django.contrib.auth.password_validation import validate_password
from django.core import exceptions

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    confirm = serializers.CharField(write_only=True)

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'first_name', 'last_name', 'created_at', 'updated_at', 'is_staff', 'password', 'confirm')


    def validate(self, attrs):
        user_attrs = dict(**attrs).pop("confirm")
        user = WallUser(user_attrs)
        errors = dict()
        try:
            validate_password(password=attrs["password"],user=user)
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)
        if errors:
            raise serializers.ValidationError(errors)

        if attrs["password"]!=attrs["confirm"]:
            raise serializers.ValidationError({"password":["Passwords must match"]})

        return attrs;

    def create(self, validated_data):
        validated_data.pop("confirm")
        user=super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user._auth_token_cache

class MyAuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label=_("Email"))
    password = serializers.CharField(
        label=_("Password"),
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(request=self.context.get('request'),
                                email=email, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
