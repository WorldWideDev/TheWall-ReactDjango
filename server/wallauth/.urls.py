from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^try/(?P<rType>\d+)$', views.logreg),
    url(r'^logout$', views.log_out),
    url(r'^pw_change$', views.change_pw),
    url(r'^users$', views.ListUsers.as_view()),
    url(r'^user/edit/(?P<pk>\d+)$', views.UpdateUser.as_view(), name='edit'),
    url(r'^user/show/(?P<pk>\d+)$', views.DetailUser.as_view(), name='show')
]
