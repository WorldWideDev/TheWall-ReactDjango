from django.forms import ModelForm, ValidationError
from django.core import validators
from django import forms
from .models import WallUser
from datetime import datetime
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _

# CUSTOM DATE INPUT
class DateInput(forms.DateInput):
    input_type = 'date'

class NameField(forms.CharField):
    MIN_LENGTH = 3
    NAME_REGEX = r'^[a-zA-Z]+$'
    NameMinLengthValidator = validators.MinLengthValidator(
        MIN_LENGTH,
        "Name Fields must be %(limit_value)d or more characters long, you have %(show_value)d"
    )
    NameRegexValidator = validators.RegexValidator(
        NAME_REGEX,
        "Names must only contain letter characters"
    )
    def __init__(self, *args, **kwargs):
        super(NameField, self).__init__(*args, **kwargs)
        self.validators.append(NameField.NameMinLengthValidator)
        self.validators.append(NameField.NameRegexValidator)


class UpdateUser(forms.ModelForm):
    def __init__(self, *args, **kargs):
        super(UpdateUser, self).__init__(*args, **kargs)

    first_name = NameField()
    last_name = NameField()

    class Meta:
        model = WallUser
        fields = (
            'first_name',
            'last_name',
            'email',
        )

class RegistrationForm(UserCreationForm):

    def __init__(self, *args, **kargs):
        super(RegistrationForm, self).__init__(*args, **kargs)

    first_name = NameField()
    last_name = NameField()

    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class': 'form-control'
        }),
        label="Password"
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class': 'form-control'
        }),
        label="Confirm Password"
    )

    class Meta:
        model = WallUser
        fields = [
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        ]
