from rest_framework import serializers
from .models import Comment
from rest_framework.validators import ValidationError
from ..wallauth.serializers import UserSerializer
from ..wall_messages.serializers import MessageSerializer
from django.contrib.auth import get_user_model

class CommentSerializer(serializers.ModelSerializer):
    MIN_LENGTH = 5
    creator_id = serializers.IntegerField(write_only=True)
    message_id = serializers.IntegerField(write_only=True)
    creator = UserSerializer(read_only=True)
    message = MessageSerializer(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"

    def validate_content(self, value):
        if len(value) < 5:
            raise ValidationError("Message too short (must be %s characters or longer)" % self.MIN_LENGTH)
        return value

    def create(self, validated_data, **kwargs):
        print("BOUT TO CREATE SOME SHIT")
        print(validated_data)
        comment=super(CommentSerializer, self).create(validated_data)
        comment.save()
        return comment 

        


    
