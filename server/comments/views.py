from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated
from .models import Comment
from .serializers import CommentSerializer
from rest_framework import generics, mixins

class CommentList(mixins.ListModelMixin,
                mixins.CreateModelMixin,
                generics.GenericAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        request.data['creator_id'] = request.user.id
        return self.create(request, *args, **kwargs)
