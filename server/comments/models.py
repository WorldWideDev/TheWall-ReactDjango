from django.db import models
from ..wall_messages.models import Message
from ..wallauth.models import WallUser

class Comment(models.Model):
    content = models.TextField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(WallUser, related_name="comments")
    message = models.ForeignKey(Message, related_name="comments")

