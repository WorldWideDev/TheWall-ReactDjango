from rest_framework import serializers
from .models import Message
from rest_framework.validators import ValidationError
from ..wallauth.serializers import UserSerializer
from django.contrib.auth import get_user_model

class MessageSerializer(serializers.ModelSerializer):
    MIN_LENGTH = 5
    creator_id = serializers.IntegerField(write_only=True)
    creator = UserSerializer(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    class Meta:
        model = Message
        fields = "__all__"

    def validate_content(self, value):
        if len(value) < 5:
            raise ValidationError("Message too short (must be %s characters or longer)" % self.MIN_LENGTH)
        return value
    def create(self, validated_data, **kwargs):
        #validated_data["creator_id"] = get_user_model().id
        print("BOUT TO CREATE SOME SHIT")
        print(validated_data)
        message=super(MessageSerializer, self).create(validated_data)
        message.save()
        return message



        


    
