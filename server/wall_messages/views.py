from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated
from .models import Message
from .serializers import MessageSerializer
from rest_framework import generics, mixins

class MessageList(mixins.ListModelMixin,
                mixins.CreateModelMixin,
                generics.GenericAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)


    def get(self, request, *args, **kwargs):
        print("SUP")
        return self.list(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        # intercepting request and setting user.id so serializer doesn't get mad
        print(request.data, "is in MessageList")
        request.data['creator_id'] = request.user.id
        return self.create(request, *args, **kwargs)
