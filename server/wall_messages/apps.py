from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = 'wall_messages'
