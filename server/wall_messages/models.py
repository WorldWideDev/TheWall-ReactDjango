from django.db import models
from ..wallauth.models import WallUser
# Create your models here.
class Message(models.Model):
    content = models.TextField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True) 
    creator = models.ForeignKey(WallUser, related_name="messages")

    def __str__(self):
        return "Message #{} (-{})".format(self.id, self.creator.email)
